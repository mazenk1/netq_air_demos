# NVIDIA NetQ with NVL4 switches
This environment demonstrates NetQ support for NVL4 switches.

It simulates 2 NVL4 switches and 1 NVL4 GPU node that can be added to netq via domain wizard, with the following content of IP file:


````
# air_ip_file
192.168.200.2 0 nvlink_switch
192.168.200.3 1 nvlink_switch
192.168.200.4 1 nvlink_l1_switch
````


## SSH access credentials

- oob mgmt server credentials: ````ubuntu / netq_123````
- nvlink switches credentials: ````root / centos````
- Netq-ts credentials: ````cumulus / CumulusLinux!````
# NVIDIA NetQ with 100 NVL4 switches
This environment demonstrates NetQ support for NVL4 switches.

It simulates 60 NVL4 switches and 40 NVL4 GPU nodes that can be added to netq via domain wizard, with the following content of IP file:


````
# air_ip_file
192.168.200.2 0 nvlink-switch
192.168.200.3 1 nvlink-switch
192.168.200.4 2 nvlink-switch
192.168.200.5 3 nvlink-switch
192.168.200.6 4 nvlink-switch
192.168.200.7 5 nvlink-switch
192.168.200.8 6 nvlink-switch
192.168.200.9 7 nvlink-switch
192.168.200.10 8 nvlink-switch
192.168.200.11 9 nvlink-switch
192.168.200.12 10 nvlink-switch
192.168.200.13 11 nvlink-switch
192.168.200.14 12 nvlink-switch
192.168.200.15 13 nvlink-switch
192.168.200.16 14 nvlink-switch
192.168.200.17 15 nvlink-switch
192.168.200.18 16 nvlink-switch
192.168.200.19 17 nvlink-switch
192.168.200.20 18 nvlink-switch
192.168.200.21 19 nvlink-switch
192.168.200.22 20 nvlink-switch
192.168.200.23 21 nvlink-switch
192.168.200.24 22 nvlink-switch
192.168.200.25 23 nvlink-switch
192.168.200.26 24 nvlink-switch
192.168.200.27 25 nvlink-switch
192.168.200.28 26 nvlink-switch
192.168.200.29 27 nvlink-switch
192.168.200.30 28 nvlink-switch
192.168.200.31 29 nvlink-switch
192.168.200.32 30 nvlink-switch
192.168.200.33 31 nvlink-switch
192.168.200.34 32 nvlink-switch
192.168.200.35 33 nvlink-switch
192.168.200.36 34 nvlink-switch
192.168.200.37 35 nvlink-switch
192.168.200.38 36 nvlink-switch
192.168.200.39 37 nvlink-switch
192.168.200.40 38 nvlink-switch
192.168.200.41 39 nvlink-switch
192.168.200.42 40 nvlink-switch
192.168.200.43 41 nvlink-switch
192.168.200.44 42 nvlink-switch
192.168.200.45 43 nvlink-switch
192.168.200.46 44 nvlink-switch
192.168.200.47 45 nvlink-switch
192.168.200.48 46 nvlink-switch
192.168.200.49 47 nvlink-switch
192.168.200.50 48 nvlink-switch
192.168.200.51 49 nvlink-switch
192.168.200.52 50 nvlink-switch
192.168.200.53 51 nvlink-switch
192.168.200.54 52 nvlink-switch
192.168.200.55 53 nvlink-switch
192.168.200.56 54 nvlink-switch
192.168.200.57 55 nvlink-switch
192.168.200.58 56 nvlink-switch
192.168.200.59 57 nvlink-switch
192.168.200.60 58 nvlink-switch
192.168.200.61 59 nvlink-switch
192.168.200.62 60 nvlink-l1-switch
192.168.200.63 61 nvlink-l1-switch
192.168.200.64 62 nvlink-l1-switch
192.168.200.65 63 nvlink-l1-switch
192.168.200.66 64 nvlink-l1-switch
192.168.200.67 65 nvlink-l1-switch
192.168.200.68 66 nvlink-l1-switch
192.168.200.69 67 nvlink-l1-switch
192.168.200.70 68 nvlink-l1-switch
192.168.200.71 69 nvlink-l1-switch
192.168.200.72 70 nvlink-l1-switch
192.168.200.73 71 nvlink-l1-switch
192.168.200.74 72 nvlink-l1-switch
192.168.200.75 73 nvlink-l1-switch
192.168.200.76 74 nvlink-l1-switch
192.168.200.77 75 nvlink-l1-switch
192.168.200.78 76 nvlink-l1-switch
192.168.200.79 77 nvlink-l1-switch
192.168.200.80 78 nvlink-l1-switch
192.168.200.81 79 nvlink-l1-switch
192.168.200.82 80 nvlink-l1-switch
192.168.200.83 81 nvlink-l1-switch
192.168.200.84 82 nvlink-l1-switch
192.168.200.85 83 nvlink-l1-switch
192.168.200.86 84 nvlink-l1-switch
192.168.200.87 85 nvlink-l1-switch
192.168.200.88 86 nvlink-l1-switch
192.168.200.89 87 nvlink-l1-switch
192.168.200.90 88 nvlink-l1-switch
192.168.200.91 89 nvlink-l1-switch
192.168.200.92 90 nvlink-l1-switch
192.168.200.93 91 nvlink-l1-switch
192.168.200.94 92 nvlink-l1-switch
192.168.200.95 93 nvlink-l1-switch
192.168.200.96 94 nvlink-l1-switch
192.168.200.97 95 nvlink-l1-switch
192.168.200.98 96 nvlink-l1-switch
192.168.200.99 97 nvlink-l1-switch
192.168.200.100 98 nvlink-l1-switch
192.168.200.101 99 nvlink-l1-switch
````


## SSH access credentials

- oob mgmt server credentials: ````ubuntu / netq_123````
- nvlink switches credentials: ````root / centos````
- Netq-ts credentials: ````cumulus / CumulusLinux!````